#ifndef WYRAZENIEZESP_HH
#define WYRAZENIEZESP_HH
#include "LZespolona.hh"
#include <iostream>
#include <cmath>
#include <climits>
#include <cstring>
#include <cassert>
using namespace std;

/*!                                                                                                                                                                              
 * Modeluje zbior operatorow arytmetycznych.                                                                                                                                     
 */
enum opperator { 
    Op_Dodaj, 
    Op_Odejmij, 
    Op_Mnoz, 
    Op_Dziel, 
    Op_Modulo 
};

/*                                                                                                                                                                               
 * Modeluje pojecie dwuargumentowego wyrazenia zespolonego                                                                                                                       
 */
struct WyrazenieZesp {
  LZespolona   Arg1;   // Pierwszy argument wyrazenia arytmetycznego                                                                                                             
  opperator     Op;     // Opertor wyrazenia arytmetycznego                                                                                                                       
  LZespolona   Arg2;   // Drugi argument wyrazenia arytmetycznego                                                                                                                
};

void Wyswietl(WyrazenieZesp  WyrZ); // wyswietlenie wyrazenia zespolonego                                                                                                        
LZespolona Oblicz(WyrazenieZesp  WyrZ); //obliczenie wyrazenia zepspolonego                                                                                                      

#endif

