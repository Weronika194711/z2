#include "BazaTestu.hh"
#include "LZespolona.hh"
#include "Statystyka.hh"
#include "WyrazenieZesp.hh"
#include <iostream>
#include <cmath>
#include <climits>
#include <cstring>
#include <cassert>
using namespace std;


using namespace std;

int main(int argc, char **argv)
{
  int a;
  LZespolona odpowiedz, poprawna;
  Statystyka statystyka;

  if (argc < 2) {
    cout << endl;
    cout << " Brak opcji okreslajacej rodzaj testu." << endl;
    cout << " Dopuszczalne nazwy to:  latwy, trudny." << endl;
    cout << endl;
    return 1;
  }


  BazaTestu   BazaT = { nullptr, 0, 0 };

  if (InicjalizujTest(&BazaT,argv[1]) == false) {
    cerr << " Inicjalizacja testu nie powiodla sie." << endl;
    return 1;
  }



  cout << endl;
  cout << " Start testu arytmetyki zespolonej: " << argv[1] << endl;
  cout << endl;

  WyrazenieZesp   WyrZ_PytanieTestowe;

  while (PobierzNastpnePytanie(&BazaT,&WyrZ_PytanieTestowe)) {
    cout << ":? Podaj wynik operacji: ";
    Wyswietl(WyrZ_PytanieTestowe);
    cout << " Twoja odpowiedz: ";
    cin>>odpowiedz;
    poprawna=Oblicz(WyrZ_PytanieTestowe);
    a=sprawdzenie(poprawna, odpowiedz);
    if(a==1){
      statystyka.poprawna++;
    }
    else{
      cout<<"Blad. Prawidlowym wynikiem jest: ";
 cout<<poprawna<<endl;
 statystyka.niepoprawna++;
    }
    cout<<endl;
  }


  cout << endl;
  cout << " Koniec testu" << endl;
  cout << endl;
  Odpowiedz(statystyka);

}