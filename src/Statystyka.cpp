#include "BazaTestu.hh"
#include "LZespolona.hh"
#include "Statystyka.hh"
#include "WyrazenieZesp.hh"
#include <iostream>
#include <cmath>
#include <climits>
#include <cstring>
#include <cassert>
using namespace std;

/*!                                                                                                                                                                              
 * Realizuje prowadzenie statystyk odpowiedzi.                                                                                                                                   
 * Argumenty:                                                                                                                                                                    
 *    odpowiedz - zmienna, zawierająca inforamcje o liczbie popranwych i niepoprawnych odpowiedzi.                                                                               
 */
void Odpowiedz (Statystyka odpowiedz){
  odpowiedz.procenty=(odpowiedz.poprawna/(odpowiedz.poprawna + odpowiedz.niepoprawna))*100;
  cout<<"Ilosc dobrych odpowiedzi:  "<<odpowiedz.poprawna<<endl;
  cout<<"Ilosc blednych odpowiedzi: "<<odpowiedz.niepoprawna<<endl;
  cout<<"Wynik procentowy poprawnych odpowiedzi:"<<odpowiedz.procenty<<"% "<<endl;


}

