#include "BazaTestu.hh"
#include "LZespolona.hh"
#include "Statystyka.hh"
#include "WyrazenieZesp.hh"
#include <iostream>
#include <cmath>
#include <climits>
#include <cstring>
#include <cassert>
using namespace std;


/*!
 * Realizuje przeciazanie operatora dla wczytywania liczby zespolonej.
 * Argumenty:
 *    StrWe- strumien wejscia
 *    k - argument, ktory funkcja ma wczytac,
 *Zwraca:
 *      Referencje do obiektu istream
 */
std:: istream& operator>>(istream &StrmWe, LZespolona &k){
    char s[3];
    StrmWe>>s[0]>>k.re>>k.im>>s[1]>>s[2];
    for (int i=2;((s[0]!='('||s[1]!='i'||s[2]!=')')&&(i>0));i--){

        cout << "Blad wprowadzenia liczby. Liczba pozostalych prob: "<<i<<" Sprobuj jeszcze raz:";
        cin.clear();
        cin.ignore(INT_MAX,'\n');
        StrmWe>>s[0]>>k.re>>k.im>>s[1]>>s[2];

    }
    //StrmWe>>k.re>>k.im;
    return StrmWe;
}


/*!
 * Realizuje przeciazanie operatora dla wyswietlania liczby zespolonej.
 * Argumenty:
 *    StrWy- strumien wyjscia
 *    k - argument, ktory funkcja ma wyswietlic,
 *Zwraca:
 *      Referencje do obiektu ostream
 */
std:: ostream& operator<<(ostream &StrmWy, LZespolona k){

    return StrmWy<<"("<<k.re<<showpos<<k.im<<"i)"<<noshowpos;
}
/*!                                                                                                                                                                              
 * Realizuje sprzezenie liczb zespolonej.                                                                                                                                        
 * Argumenty:                                                                                                                                                                    
 *    Skl2 - liczba, ktorej uzyskamy sprzezenie.                                                                                                                                 
 * Zwraca:                                                                                                                                                                       
 *    Liczbe sprzezona do wskazanego argumentu.                                                                                                                                  
 */
LZespolona  Sprzezenie(LZespolona  Skl2)
{
  LZespolona  Wynik;

  Wynik.re = Skl2.re;
  Wynik.im = - Skl2.im;

  return Wynik;
}

/*!                                                                                                                                                                              
 * Realizuje uzyskanie modulu liczby zespolonej.                                                                                                                                 
 * Argumenty:                                                                                                                                                                    
 *    Skl2 - liczba, ktorej modul uzyskamy.                                                                                                                                      
 * Zwraca:                                                                                                                                                                       
 *    Modul wskazanego argumentu.                                                                                                                                                
 */
double  Modul2(LZespolona  Skl2)
{
  double modul=0;
  modul=sqrt(Skl2.re*Skl2.re + Skl2.im*Skl2.im);
  return modul;
}


/*!                                                                                                                                                                              
 * Realizuje dodanie dwoch liczb zespolonych.                                                                                                                                    
 * Argumenty:                                                                                                                                                                    
 *    Skl1 - pierwszy skladnik dodawania,                                                                                                                                        
 *    Skl2 - drugi skladnik dodawania.                                                                                                                                           
 * Zwraca:                                                                                                                                                                       
 *    Sume dwoch skladnikow przekazanych jako parametry.                                                                                                                         
 */
LZespolona  operator + (LZespolona  Skl1,  LZespolona  Skl2)
{
  LZespolona  Wynik;

  Wynik.re = Skl1.re + Skl2.re;
  Wynik.im = Skl1.im + Skl2.im;

  return Wynik;
}

/*!                                                                                                                                                                              
 * Realizuje odejmowanie dwoch liczb zespolonych.                                                                                                                                
 * Argumenty:                                                                                                                                                                    
 *    Skl1 - pierwszy skladnik odejmowania,                                                                                                                                      
 *    Skl2 - drugi skladnik odejmowania.                                                                                                                                         
 * Zwraca:                                                                                                                                                                       
 *    Roznice dwoch skladnikow przekazanych jako parametry.                                                                                                                      
 */
LZespolona  operator - (LZespolona  Skl1,  LZespolona  Skl2)
{
  LZespolona  Wynik;

  Wynik.re = Skl1.re - Skl2.re;
  Wynik.im = Skl1.im - Skl2.im;

  return Wynik;
}

/*!                                                                                                                                                                              
 * Realizuje mnozenie dwoch liczb zespolonych.                                                                                                                                   
 * Argumenty:                                                                                                                                                                    
 *    Skl1 - pierwszy skladnik mnozenia,                                                                                                                                         
 *    Skl2 - drugi skladnik mnozenia.                                                                                                                                            
 * Zwraca:                                                                                                                                                                       
 *    Iloczyn dwoch skladnikow przekazanych jako parametry.                                                                                                                      
 */
LZespolona  operator * (LZespolona  Skl1,  LZespolona  Skl2)
{
  LZespolona  Wynik;

  Wynik.re = Skl1.re*Skl2.re - Skl1.im*Skl2.im;
  Wynik.im = Skl1.re*Skl2.im + Skl1.im*Skl2.re;

  return Wynik;
}

/*!                                                                                                                                                                              
 * Realizuje dzielenie modulo dwoch liczb zespolonych.                                                                                                                           
 * Argumenty:                                                                                                                                                                    
 *    Skl1 - pierwszy skladnik dzielenia modulo,                                                                                                                                         
 *    Skl2 - drugi skladnik dzielenia modulo.                                                                                                                                            
 * Zwraca:                                                                                                                                                                       
 *    Reszte z dzielenia dwoch skladnikow przekazanych jako parametry.                                                                                                                      
 */
LZespolona  operator % (LZespolona  Skl1,  LZespolona  Skl2)
{
  LZespolona  Wynik;
int re1, re2, im1, im2, wynikre, wynikim;

/*konwersja typu do int aby otrzymac liczby na ktorych mozna wykonac dzielenie calkowitoliczbowe*/
re1=static_cast<int>(Skl1.re);
re2=static_cast<int>(Skl2.re);
im1=static_cast<int>(Skl1.im);
im2=static_cast<int>(Skl2.im);
/*uzyskalismy liczby calkowite jako wspolczynniki przy czesciah rzeczywistych i urojonych*/

wynikre=re1%re2;
wynikim=im1%im2;

/*zmiana typu wyniku*/
Wynik.re=static_cast<double>(wynikre);
Wynik.im=static_cast<double>(wynikim);
  return Wynik;
}

/*!                                                                                                                                                                              
 * Realizuje dzielenie przez liczbe zespolona.                                                                                                                                   
 * Argumenty:                                                                                                                                                                    
 *    Skl1 - pierwszy skladnik dzielenia,                                                                                                                                        
 *    Skl2 - drugi skladnik dzielenia.                                                                                                                                           
 * Zwraca:                                                                                                                                                                       
 *    Iloraz dwoch skladnikow przekazanych jako parametry.                                                                                                                       
 */
LZespolona  operator / (LZespolona  Skl1,  LZespolona  Skl2)
{
  LZespolona  Wynik;
  LZespolona sprzezenie;
  double modul;

  sprzezenie=Sprzezenie(Skl2);
  modul=Modul2(Skl2);

  if(modul > 0.0){
    Wynik=(Skl1 * sprzezenie)/(modul * modul);
  }
  else{
    cout<<"Modul liczby zespolonej wynosi 0, wiec dzielenie jest niewykoanlne."<<endl;
  }

  return Wynik;
}

/*!                                                                                                                                                                              
 * Realizuje dzielenie przez liczbe calkowita.                                                                                                                                   
 * Argumenty:                                                                                                                                                                    
 *    Skl1 - pierwszy skladnik dzielenia,                                                                                                                                        
 *    k - drugi skladnik dzielenia.                                                                                                                                              
 * Zwraca:                                                                                                                                                                       
 *    Iloraz dwoch skladnikow przekazanych jako parametry.                                                                                                                       
 */
LZespolona  operator / (LZespolona  Skl1,  double k)
{
  LZespolona  wynik;

  if(k>0.0){
  wynik.re = Skl1.re/k;
  wynik.im = Skl1.im/k;
  }
  else{
    cout<<"Dzialanie jest niewykonlne, nie mozna dzielic przez zero"<<endl;
  }
}
/*!                                                                                                                                                                              
 * Realizuje sprawdzenie poprawnosci wykonanego dzialania na liczbach zespolonych.                                                                                               
 * Argumenty:                                                                                                                                                                    
 *    poprawna - poprawne wykonanie obliczenia,                                                                                                                                  
 *    odpowiedz - odpowiedz podana przez uzytkownika.                                                                                                                            
 * Zwraca:                                                                                                                                                                       
 *    0 dla błędnej odpowiedzi                                                                                                                                                   
 *    1 dla poprawnej odpowiedzi.                                                                                                                                                
 */
int sprawdzenie(LZespolona poprawna, LZespolona odpowiedz){
 
  
 int punkt;

 if(Modul2(poprawna-odpowiedz)==0){
   cout<<":) Odpowiedz poprawna"<<endl;
   punkt=1;
 }
 else{
    cout<<":( Blad. Prawidlowym wynikiem jest:    "<<endl; 
   punkt=0;
 }

 return punkt;
}
