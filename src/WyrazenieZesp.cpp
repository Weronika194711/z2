#include "BazaTestu.hh"
#include "LZespolona.hh"
#include "Statystyka.hh"
#include "WyrazenieZesp.hh"
#include <iostream>
#include <cmath>
#include <climits>
#include <cstring>
#include <cassert>
using namespace std;

/*!                                                                                                                                                                              
 * Realizuje wyswietlenie wyrazenia zespolonego.                                                                                                                                 
 * Argumenty:                                                                                                                                                                    
 *    WyrZ - dzialanie do wyswietlenia w tescie.                                                                                                                                 
 */
void Wyswietl(WyrazenieZesp  WyrZ){
    cout<<WyrZ.Arg1;
    cout<<" ";
    switch(WyrZ.Op){
        case 0: cout<<"+"; break;
        case 1: cout<<"-"; break;
        case 2: cout<<"*"; break;
        case 3: cout<<"/"; break;
        case 4: cout<<"%"; break;
    }
    cout<<" ";
    cout<<WyrZ.Arg2;
    cout<<" = "<<endl;

}

/*!                                                                                                                                                                              
 * Realizuje obliczenie wyrazenia zepspolonego.                                                                                                                                  
 * Argumenty:                                                                                                                                                                    
 *    WyrZ - dzialanie, ktore ma funckja wykonac.                                                                                                                                
 * Zwraca:                                                                                                                                                                       
 *    Obliczony wynik.                                                                                                                                                           
 */
LZespolona Oblicz(WyrazenieZesp  WyrZ){
    LZespolona Wynik;
    switch(WyrZ.Op){
        case Op_Dodaj:{
            Wynik=WyrZ.Arg1 + WyrZ.Arg2;
            break;
        }
        case Op_Odejmij:{
            Wynik=WyrZ.Arg1 - WyrZ.Arg2;
            break;
        }
        case Op_Mnoz:{
            Wynik=WyrZ.Arg1 * WyrZ.Arg2;
            break;
        }
        case Op_Dziel:{
            Wynik=WyrZ.Arg1 / WyrZ.Arg2;
            break;
        }
        case Op_Modulo:{
            Wynik=WyrZ.Arg1 % WyrZ.Arg2;
            break;
        }
        default: cout<<"Blad. Nie ma takiego dzialania"<<endl;
    }
    return Wynik;
    

}

